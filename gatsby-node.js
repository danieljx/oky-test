/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

// You can delete this file if you're not using it

const path = require('path')

exports.createPages = async ({ graphql, actions }) => {
 
    const { createPage } = actions
    const productTemplate = path.resolve(`src/templates/Product.js`)
    const results = await graphql(`
        query ShipsQuery {
            allStarshipsJson {
                edges {
                    node {
                        id,
                        name,
                        model,
                        images,
                        manufacturer,
                        cost_in_credits,
                        max_atmosphering_speed,
                        crew,
                        passengers,
                        cargo_capacity,
                        consumables,
                        hyperdrive_rating,
                        MGLT,
                        starship_class,
                        pilots,
                        films,
                        created,
                        localImage {
                            absolutePath
                            publicURL
                        }
                    } 
                }
            }
        }
    `)

    if (results.errors) {
        throw results.errors
    }
 
    results.data.allStarshipsJson.edges.forEach(({ node }) => {
        createPage({
            path: `product/${node.id}`,
            component: productTemplate,
            context: node,
        });
    });
}