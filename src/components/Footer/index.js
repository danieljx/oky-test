import React from 'react'
import PropTypes from "prop-types"
import { Row, Col } from "react-bootstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBitbucket } from '@fortawesome/free-brands-svg-icons'
import "./style.css"

const Footer = ({ author, authorEmail, authorBitbucket }) => {
    return (
        <footer id="okyFooter" className="bg-dark container-fluid">
            <Row>
                <Col md={6}>
                    <div className="text-center text-light">
                        © {new Date().getFullYear()} Copyright{` `}
                        <a className="text-light" href={`mailto:${authorEmail}`} target="_blank" rel="noopener noreferrer">{author}</a>
                    </div>
                </Col>
                <Col md={6}>
                    <div className="text-center text-light">
                        <a className="text-light" href={authorBitbucket} target="_blank" rel="noopener noreferrer">
                            <FontAwesomeIcon icon={faBitbucket} />
                        </a>
                    </div>
                </Col>
            </Row>
        </footer>
    )
}

Footer.propTypes = {
    author: PropTypes.string.isRequired,
    authorEmail: PropTypes.string.isRequired,
    authorBitbucket: PropTypes.string.isRequired,
}
  
export default Footer
