import * as React from "react"
import { Link } from "gatsby"
import { Navbar, Image } from "react-bootstrap"
import "./style.css"
import OKYLogoSVG from "../../images/OKY-logo.svg"

const Header = () => (
  <Navbar id="okyHeader" variant="dark" expand="lg" className="pl-md-5 pr-md-5" fixed="top">
    <Navbar.Brand as={Link} to="/" className="mr-5">
        <Image
            src={OKYLogoSVG}
            width={100}
            alt="OKY"
        />
    </Navbar.Brand>
</Navbar>
)

export default Header
