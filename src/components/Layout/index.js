 import * as React from "react"
 import PropTypes from "prop-types"
 import { useStaticQuery, graphql } from "gatsby"
 import 'bootstrap/dist/css/bootstrap.min.css'
 import { Container } from "react-bootstrap"
 import Header from "../Header"
 import Footer from "../Footer"
 import "./style.css"
 
 const Layout = ({ children }) => {
   const dataSite = useStaticQuery(graphql`
    query SiteQuery {
      site {
        siteMetadata {
          title
          author
          author_email
          author_bitbucket
        }
      }
    }
   `)
   return (
     <>
        <Header siteTitle={dataSite.site.siteMetadata.title} />
        <Container id="okyContainer">{children}</Container>
        <Footer
          author={dataSite.site.siteMetadata.author}
          authorEmail={dataSite.site.siteMetadata.author_email}
          authorBitbucket={dataSite.site.siteMetadata.author_bitbucket}
        />
     </>
   )
 }
 
 Layout.propTypes = {
   children: PropTypes.node.isRequired,
 }
 
 export default Layout
 