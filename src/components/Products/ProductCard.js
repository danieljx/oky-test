import React from 'react'
import PropTypes from 'prop-types'
import { Link } from "gatsby"
import { Card, Image, Carousel } from "react-bootstrap"
import formatCurrency from "../../utils/formatCurrency"
import "./styleProductCard.css"

const ProductCard = ({ id, name, costInCredits, images, localImage, model }) => {
    return (
        <Link to={`product/${id}`}>
            <Card className="product-card">
                <Carousel>
                    {images.length > 0 && images.map((img, i) => {
                        return (
                            <Carousel.Item key={`${id}-${i.toString()}-img`}>
                                <Image
                                    className="d-block w-100"
                                    src={img}
                                    alt={name}
                                />
                            </Carousel.Item>
                        );
                    })}
                </Carousel>
                <Card.Body>
                    <Card.Title className="name">{name}</Card.Title>
                    <Card.Title className="price">{formatCurrency(costInCredits)}</Card.Title>
                    <Card.Subtitle className="model">
                        <strong>Model:{` `}</strong>
                        {model}
                    </Card.Subtitle>
                </Card.Body>
            </Card>
        </Link>
    )
}

ProductCard.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    costInCredits: PropTypes.number.isRequired,
    images: PropTypes.array.isRequired,
    localImage: PropTypes.array.isRequired,
    manufacturer: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
}

export default ProductCard

