import React from 'react'
import PropTypes from 'prop-types'
import { Row, Col } from "react-bootstrap"
import ProductCard from './ProductCard'
import "./style.css"

const Products = ({ edges }) => {
    return (
        <Row id="okyProducts">
            {edges && edges.map((edge, i) => (
                <Col xs={12} sm={12} md={3}>
                    <ProductCard
                        key={`${edge.node.id}-${i.toString()}-product`}
                        id={edge.node.id}
                        costInCredits={parseFloat(edge.node.cost_in_credits)}
                        name={edge.node.name}
                        images={edge.node.images}
                        localImage={edge.node.localImage}
                        model={edge.node.model}
                    />
                </Col>
            ))}
        </Row>
    )
}

Products.propTypes = {
    edges: PropTypes.array.isRequired,
}

export default Products

