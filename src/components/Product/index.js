import React from 'react'
import PropTypes from 'prop-types'
import { Card, Image, Carousel, Row, Col, Container, Table, ListGroup } from "react-bootstrap"
import Seo from "../Seo"
import formatCurrency from "../../utils/formatCurrency"
import "./style.css"

const Product = ({ id, name, costInCredits, images, localImage, manufacturer, model, maxAtmospheringSpeed, crew, passengers, consumables, hyperdriveRating, mglt, starshipClass, pilots, films, created }) => {
    return (
        <>
            <Seo title={name} />
            <Container id="okyProduct" className="pt-4">
                <Card>
                    <Card.Body>
                        <Row>
                            <Col md={8} className="right">
                                <Carousel>
                                    {images.length > 0 && images.map((img, i) => {
                                        return (
                                            <Carousel.Item key={`${id}-${i.toString()}-img`}>
                                                <Image
                                                    className="d-block w-100"
                                                    src={img}
                                                    alt={name}
                                                />
                                            </Carousel.Item>
                                        );
                                    })}
                                </Carousel>
                                <Card className="d-none d-md-block">
                                    <Card.Header>Data sheet</Card.Header>
                                    <Table striped bordered hover>
                                        <tbody>
                                            <tr>
                                                <td>Max atmosphering speed</td>
                                                <td>{maxAtmospheringSpeed}</td>
                                            </tr>
                                            <tr>
                                                <td>Crew</td>
                                                <td>{crew}</td>
                                            </tr>
                                            <tr>
                                                <td>Passengers</td>
                                                <td>{passengers}</td>
                                            </tr>
                                            <tr>
                                                <td>Consumables</td>
                                                <td>{consumables}</td>
                                            </tr>
                                            <tr>
                                                <td>Hyperdrive rating</td>
                                                <td>{hyperdriveRating}</td>
                                            </tr>
                                            <tr>
                                                <td>MGLT</td>
                                                <td>{mglt}</td>
                                            </tr>
                                            <tr>
                                                <td>Starship class</td>
                                                <td>{starshipClass}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                            <Col md={4} className="left">
                                <Card className="mt-0">
                                    <Card.Body>
                                        <Card.Title className="name">{name}</Card.Title>
                                        <Card.Title className="price">{formatCurrency(costInCredits)}</Card.Title>
                                        <Card.Subtitle className="model">
                                            <strong>Model:{` `}</strong>
                                            {model}
                                        </Card.Subtitle>
                                        <Card.Subtitle className="manufacturer">
                                            <strong>Manufacturer:{` `}</strong>
                                            {manufacturer}
                                        </Card.Subtitle>
                                    </Card.Body>
                                </Card>
                                {films.length > 0 && (
                                    <Card>
                                        <Card.Header>Films</Card.Header>
                                        <ListGroup variant="flush">
                                            {films.map((film, i) => (
                                                <ListGroup.Item key={`${id}-${i.toString()}-film`}>{film}</ListGroup.Item>
                                            ))}
                                        </ListGroup>
                                    </Card>
                                )}
                                {pilots.length > 0 && (
                                    <Card>
                                        <Card.Header>Pilots</Card.Header>
                                        <ListGroup variant="flush">
                                            {pilots.map((pilot, i) => (
                                                <ListGroup.Item key={`${id}-${i.toString()}-pilot`}>{pilot}</ListGroup.Item>
                                            ))}
                                        </ListGroup>
                                    </Card>
                                )}
                                <Card className="d-md-none">
                                    <Card.Header>Data sheet</Card.Header>
                                    <Table striped bordered hover>
                                        <tbody>
                                            <tr>
                                                <td>Max atmosphering speed</td>
                                                <td>{maxAtmospheringSpeed}</td>
                                            </tr>
                                            <tr>
                                                <td>Crew</td>
                                                <td>{crew}</td>
                                            </tr>
                                            <tr>
                                                <td>Passengers</td>
                                                <td>{passengers}</td>
                                            </tr>
                                            <tr>
                                                <td>Consumables</td>
                                                <td>{consumables}</td>
                                            </tr>
                                            <tr>
                                                <td>Hyperdrive rating</td>
                                                <td>{hyperdriveRating}</td>
                                            </tr>
                                            <tr>
                                                <td>MGLT</td>
                                                <td>{mglt}</td>
                                            </tr>
                                            <tr>
                                                <td>Starship class</td>
                                                <td>{starshipClass}</td>
                                            </tr>
                                        </tbody>
                                    </Table>
                                </Card>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </>
    )
}

Product.propTypes = {
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    costInCredits: PropTypes.number.isRequired,
    images: PropTypes.array.isRequired,
    localImage: PropTypes.array.isRequired,
    manufacturer: PropTypes.string.isRequired,
    model: PropTypes.string.isRequired,
    maxAtmospheringSpeed: PropTypes.string.isRequired,
    crew: PropTypes.string.isRequired,
    passengers: PropTypes.string.isRequired,
    consumables: PropTypes.string.isRequired,
    hyperdriveRating: PropTypes.string.isRequired,
    mglt: PropTypes.string.isRequired,
    starshipClass: PropTypes.string.isRequired,
    pilots: PropTypes.array.isRequired,
    films: PropTypes.array.isRequired,
    created: PropTypes.string.isRequired,
}

export default Product

