import * as React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Seo from "../components/Seo"
import { Container, Row, Col } from "react-bootstrap"
import Products from "../components/Products"

const IndexPage = ({ location }) => {
  const dataStarShips = useStaticQuery(graphql`
   query ShipsQuery {
     allStarshipsJson {
       edges {
         node {
           id,
           name,
           cost_in_credits,
           model,
           images,
           manufacturer,
           localImage {
               absolutePath
               publicURL
           }
         } 
       }
     }
   }
  `)
  return (
    <>
      <Seo title="Home" />
      <Container className="pt-4">
        <Row>
          <Col>
            <Products edges={dataStarShips.allStarshipsJson.edges} />
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default IndexPage
