const formatCurrency = (
  amount,
  type = 'en-US',
  style = 'currency',
  currency = 'USD',
  minimumFractionDigits = 0,
)  => {
  return new Intl.NumberFormat(type, {
    style,
    currency,
    minimumFractionDigits,
  }).format(amount);
};
export default formatCurrency;
