import React from 'react'
import PropTypes from 'prop-types'
import { default as ProductDetails } from "../components/Product"

const Product = ({ pageContext }) => {
    return (
        <ProductDetails {...pageContext}
            id={pageContext.id}
            costInCredits={parseFloat(pageContext.cost_in_credits)}
            images={pageContext.images}
            localImage={pageContext.localImage}
            manufacturer={pageContext.manufacturer}
            model={pageContext.model} 
            maxAtmospheringSpeed={pageContext.max_atmosphering_speed}
            crew={pageContext.crew}
            passengers={pageContext.passengers}
            consumables={pageContext.consumables}
            hyperdriveRating={pageContext.hyperdrive_rating}
            mglt={pageContext.MGLT}
            starshipClass={pageContext.starship_class}
            pilots={pageContext.pilots}
            films={pageContext.films}
            created={pageContext.created}
        />
    )
}

Product.propTypes = {
    pageContext: PropTypes.object.isRequired,
}

export default Product
